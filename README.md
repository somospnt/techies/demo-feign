# Demo Spring Cloud Feign

## ¿Qué es Feign?
Es una librería que permite generar clientes rest en tiempo de ejecución basandose en el uso de una interfaz declarativa

## Cómo correrlo:
- Correrlo de forma local. En [localhost](localhost:8080/) hay una home con el listado de todos los endpoints implememtados con feign y que consumen la [siguiente api](https://reqres.in/)

## Más info
- En los tests del proyecto se pueden ver distintas implementaciones.